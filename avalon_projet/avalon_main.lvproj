﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Property Name="varPersistentID:{1BA1384D-B53B-4409-86F2-C77C5527815F}" Type="Ref">/Poste de travail/lib.lvlib/vitesse_vent_km</Property>
	<Property Name="varPersistentID:{1DB9CD63-12B0-467A-984A-DD67F89A1B12}" Type="Ref">/Poste de travail/lib.lvlib/com_angle_GV_deg</Property>
	<Property Name="varPersistentID:{2EC9D0B1-BE83-4AF1-9941-54893325F8E1}" Type="Ref">/Poste de travail/lib.lvlib/pos_y</Property>
	<Property Name="varPersistentID:{46A4EF3B-D83A-4FD7-82F1-D4D406364F73}" Type="Ref">/Poste de travail/lib.lvlib/pos_x</Property>
	<Property Name="varPersistentID:{4F7FF6B2-43D7-4A1C-B9E6-18CDB448BE36}" Type="Ref">/Poste de travail/lib.lvlib/vitesse_gps_knots</Property>
	<Property Name="varPersistentID:{544780B3-27EE-4F54-922C-DCBDE9C23032}" Type="Ref">/Poste de travail/lib.lvlib/enable_moteurs</Property>
	<Property Name="varPersistentID:{55915419-01A4-4926-A47D-C0A5C8CB26A2}" Type="Ref">/Poste de travail/lib.lvlib/depart_x</Property>
	<Property Name="varPersistentID:{5F16512B-5326-4F5C-9B3C-F7797378C479}" Type="Ref">/Poste de travail/lib.lvlib/pos_GV_actuel_deg</Property>
	<Property Name="varPersistentID:{618DDA45-4EEE-4065-AE00-896B2C2C35C2}" Type="Ref">/Poste de travail/lib.lvlib/direction_vent_deg</Property>
	<Property Name="varPersistentID:{62B7D5BD-69B0-4199-8FB8-CA5B24062CEB}" Type="Ref">/Poste de travail/lib.lvlib/cap_actuel_deg</Property>
	<Property Name="varPersistentID:{6496D819-112F-4396-BA8D-FAB25A10C665}" Type="Ref">/Poste de travail/lib.lvlib/en_veille</Property>
	<Property Name="varPersistentID:{657F157A-6654-4C02-B719-1FB79B06997F}" Type="Ref">/Poste de travail/lib.lvlib/cap_commande</Property>
	<Property Name="varPersistentID:{7A4D8D19-7AB2-4C67-B8E0-EA81041A6E1B}" Type="Ref">/Poste de travail/lib.lvlib/pos_safran_actuel_deg</Property>
	<Property Name="varPersistentID:{8D225414-27EB-41FD-AF7F-133E1B2E0478}" Type="Ref">/Poste de travail/lib.lvlib/bateau_retourne</Property>
	<Property Name="varPersistentID:{91E84079-434F-426F-8D1E-E4980FC14ECA}" Type="Ref">/Poste de travail/lib.lvlib/waypoint_depart_actuel</Property>
	<Property Name="varPersistentID:{96E52228-EC4E-4E94-A602-BCDC0473A9F5}" Type="Ref">/Poste de travail/lib.lvlib/com_angle_safran_deg</Property>
	<Property Name="varPersistentID:{99355EB6-77AD-498F-A1DB-74B509E45B4A}" Type="Ref">/Poste de travail/lib.lvlib/longitude</Property>
	<Property Name="varPersistentID:{9A2C5E8A-B415-4353-9DB4-ED5A014B6E1B}" Type="Ref">/Poste de travail/lib.lvlib/depart_y</Property>
	<Property Name="varPersistentID:{A788C1EE-7D5B-4398-A1DA-C91624B243C5}" Type="Ref">/Poste de travail/lib.lvlib/refnum en sortie</Property>
	<Property Name="varPersistentID:{B0A9CCB9-E3E1-4C9B-ABE8-63EA5882FE4C}" Type="Ref">/Poste de travail/lib.lvlib/fin_trajet</Property>
	<Property Name="varPersistentID:{B491F0AF-CFD3-411E-9043-D3A7F0B9C1DD}" Type="Ref">/Poste de travail/lib.lvlib/periode_sortie_instrus_ms</Property>
	<Property Name="varPersistentID:{C57BC93E-2F37-4288-B1E3-679C37DEC378}" Type="Ref">/Poste de travail/lib.lvlib/utc</Property>
	<Property Name="varPersistentID:{C7B6C7DC-CBC8-4EE8-8A9A-F85E2A464AE0}" Type="Ref">/Poste de travail/lib.lvlib/th_</Property>
	<Property Name="varPersistentID:{C8C3B514-59CF-4A05-B1ED-FE3F19BA0659}" Type="Ref">/Poste de travail/lib.lvlib/niveau_charge_batterie</Property>
	<Property Name="varPersistentID:{CEF35370-8C65-49BE-81D3-55830732BDE6}" Type="Ref">/Poste de travail/lib.lvlib/lattitude</Property>
	<Property Name="varPersistentID:{D4798F2B-3217-4D3E-A1BB-2040C1A0DFD6}" Type="Ref">/Poste de travail/lib.lvlib/temps_gps</Property>
	<Property Name="varPersistentID:{D63E5226-0CB0-4053-BD12-E0A3F9A0446E}" Type="Ref">/Poste de travail/lib.lvlib/cible_y</Property>
	<Property Name="varPersistentID:{D6AB6014-493A-446D-93C6-2C0AD57A784D}" Type="Ref">/Poste de travail/lib.lvlib/cible_x</Property>
	<Item Name="Poste de travail" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">Poste de travail/VI Serveur</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">Poste de travail/VI Serveur</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="batterie.vi" Type="VI" URL="../batterie.vi"/>
		<Item Name="Compas_Avalon.vi" Type="VI" URL="../instrument/Compas_Avalon.vi"/>
		<Item Name="crc16-ccitt.vi" Type="VI" URL="../moteur/crc16-ccitt.vi"/>
		<Item Name="envoi_consigne_epos.vi" Type="VI" URL="../moteur/envoi_consigne_epos.vi"/>
		<Item Name="envoi_message.vi" Type="VI" URL="../moteur/envoi_message.vi"/>
		<Item Name="GPS(SubVI).vi" Type="VI" URL="../instrument/GPS(SubVI).vi"/>
		<Item Name="initialisations.vi" Type="VI" URL="../initialisations.vi"/>
		<Item Name="inversion_byte.vi" Type="VI" URL="../moteur/inversion_byte.vi"/>
		<Item Name="lecture_airmar_200WX.vi" Type="VI" URL="../instrument/lecture_airmar_200WX.vi"/>
		<Item Name="lib.lvlib" Type="Library" URL="../lib.lvlib"/>
		<Item Name="main.vi" Type="VI" URL="../main.vi"/>
		<Item Name="miseEnForme.vi" Type="VI" URL="../moteur/miseEnForme.vi"/>
		<Item Name="reception_donnees_epos.vi" Type="VI" URL="../moteur/reception_donnees_epos.vi"/>
		<Item Name="relative_pos_avalon.vi" Type="VI" URL="../moteur/relative_pos_avalon.vi"/>
		<Item Name="trame_compas_et_filtre.vi" Type="VI" URL="../instrument/trame_compas_et_filtre.vi"/>
		<Item Name="trame_GPGGA (sous-VI).vi" Type="VI" URL="../instrument/trame_GPGGA (sous-VI).vi"/>
		<Item Name="trame_GPVTG (sous-VI).vi" Type="VI" URL="../instrument/trame_GPVTG (sous-VI).vi"/>
		<Item Name="Trame_GPZDA (sous-VI).vi" Type="VI" URL="../instrument/Trame_GPZDA (sous-VI).vi"/>
		<Item Name="trame_HCHDT (sous-VI).vi" Type="VI" URL="../instrument/trame_HCHDT (sous-VI).vi"/>
		<Item Name="Dépendances" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/Close.vi"/>
				<Item Name="DisableAxis.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/DisableAxis.vi"/>
				<Item Name="EnableAxis.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/EnableAxis.vi"/>
				<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/Initialize.vi"/>
				<Item Name="MoveToRelativePosition.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/MoveToRelativePosition.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="NI_AAL_Angle.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AAL_Angle.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="CRC-16-CCITT-xMODEM.vi" Type="VI" URL="../moteur/Inline CRC/CRC SubVIs/Wrappers/CRC-16-CCITT-xMODEM.vi"/>
			<Item Name="Inline CRC-16-CCITT.vi" Type="VI" URL="../moteur/Inline CRC/CRC SubVIs/Inline CRC-16-CCITT.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="ProjectionGPS_XY(SubVI).vi" Type="VI" URL="../calcul_de_trajectoire(SubVI) V2/ProjectionGPS_XY(SubVI).vi"/>
			<Item Name="XDNodeRunTimeDep.lvlib" Type="Library" URL="/&lt;vilib&gt;/Platform/TimedLoop/XDataNode/XDNodeRunTimeDep.lvlib"/>
		</Item>
		<Item Name="Spécifications de construction" Type="Build"/>
	</Item>
</Project>
